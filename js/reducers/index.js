import { combineReducers } from "redux";
import appointmentsReducer from "./appointmentsReducer";
import { reducer as formReducer } from "redux-form";

const allReducers = combineReducers({
  appointmentsReducer,
  form: formReducer
});
export default allReducers;
